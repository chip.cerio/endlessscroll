package com.chipcerio.endlessscrolldemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RandomUserAdapter : RecyclerView.Adapter<RandomUserAdapter.ViewHolder>() {
    
    private var list: List<User> = emptyList()
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val root = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        return ViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }
    
    override fun getItemCount(): Int = list.size
    
    fun setItems(items: List<User>) {
        val temp = list.toMutableList()
        temp.addAll(items)
        list = temp.toList()
        notifyDataSetChanged()
    }
    
    class ViewHolder(
        private val containerView: View
    ) : RecyclerView.ViewHolder(containerView) {
        
        private val textView: TextView = containerView.findViewById(android.R.id.text1)
        
        fun bind(user: User) {
            textView.text = "${user.name.first} ${user.name.last}"
        }
    }
}