package com.chipcerio.endlessscrolldemo

import android.util.Log
import io.reactivex.Observable

class RandomUserRepository(
    private val api: RandomUserService
) : UserSource {
    
    override fun getUsers(page: Int, results: Int, seed: String): Observable<List<User>> {
        return api.getUsers(page, results, seed)
            .map { it.results }
            .doOnError { Log.e("ERR", "error in Repository", it) }
    }
}
