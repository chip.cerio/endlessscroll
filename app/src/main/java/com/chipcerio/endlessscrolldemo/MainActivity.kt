package com.chipcerio.endlessscrolldemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*

// https://guides.codepath.com/android/Endless-Scrolling-with-AdapterViews-and-RecyclerView
class MainActivity : AppCompatActivity() {
    
    private val disposables = CompositeDisposable()
    private lateinit var viewModel: ViewModel
    private lateinit var randomUserAdapter: RandomUserAdapter
    
    private val usersStream = PublishSubject.create<Int>()
    private var defaultPage = 1
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupRecyclerView()
        
        bindPagingStream()
        usersStream.onNext(defaultPage)
    }
    
    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }
    
    private fun bindPagingStream() {
        usersStream
            .observeOn(Schedulers.io())
            .concatMap { page ->
                viewModel.fetchUsers(page, MAX_ITEMS, "abc")
            }
            .observeOn(AndroidSchedulers.mainThread())
            // .doOnNext { toast("Page $it") }
            .subscribe(
                { randomUserAdapter.setItems(it) },
                { printerr("error in getting results") })
            .addTo(disposables)
    }
    
    private fun setupViewModel() {
        val service = (application as App).service()
        viewModel = ViewModel(RandomUserInjection.providesRandomUserSource(service))
    }
    
    private fun setupRecyclerView() {
        val verticalLayoutManager = LinearLayoutManager(this).apply {
            orientation = LinearLayoutManager.VERTICAL
        }
        val scrollListener = object : EndlessRecyclerViewScrollListener(verticalLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                println("page: $page")
                loadNextDataFromApi(page)
            }
        }
        randomUserAdapter = RandomUserAdapter()
        recyclerView.apply {
            layoutManager = verticalLayoutManager
            adapter = randomUserAdapter
            addOnScrollListener(scrollListener)
        }
    }
    
    private fun loadNextDataFromApi(page: Int) {
        usersStream.onNext(page)
    }
    
    companion object {
        private const val MAX_ITEMS = 15
    }
}