package com.chipcerio.endlessscrolldemo

import io.reactivex.Observable

class ViewModel(private val repository: UserSource) {
    
    fun fetchUsers(page: Int, results: Int, seed: String): Observable<List<User>> {
        return repository.getUsers(page, results, seed)
    }
}