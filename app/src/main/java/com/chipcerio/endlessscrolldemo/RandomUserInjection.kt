package com.chipcerio.endlessscrolldemo

class RandomUserInjection {
    
    companion object {
        fun providesRandomUserSource(api: RandomUserService): UserSource = RandomUserRepository(api)
    }
}
