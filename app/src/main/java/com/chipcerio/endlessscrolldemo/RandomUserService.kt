package com.chipcerio.endlessscrolldemo

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RandomUserService {
    
    // https://randomuser.me/api/?page=3&results=15&seed=abc
    @GET("/api")
    fun getUsers(
        @Query("page") page: Int,
        @Query("results") results: Int,
        @Query("seed") seed: String,
    ): Observable<UsersResult>
}