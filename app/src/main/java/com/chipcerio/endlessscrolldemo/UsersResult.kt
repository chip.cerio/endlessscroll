package com.chipcerio.endlessscrolldemo

data class UsersResult(
    val results: List<User> = emptyList(),
    val info: ResultInfo = ResultInfo()
)

data class User(
    val gender: String = "female",
    val name: Name = Name(),
    val location: Location = Location(),
    val email: String = "",
    val login: Login = Login(),
    val dob: DateOfBirth = DateOfBirth(),
    val registered: Registered = Registered(),
    val phone: String = "",
    val cell: String = "",
    val id: Id = Id(),
    val picture: Picture = Picture(),
    val nat: String = "",
)

data class ResultInfo(
    val seed: String = "",
    val results: Int = 0,
    val page: Int = 0,
    val version: String = ""
)

data class Name(
    val title: String = "",
    val first: String = "",
    val last: String = ""
)

data class Location(
    val street: Street = Street(),
    val city: String = "",
    val state: String = "",
    val country: String = "",
    val postcode: Any = Any(), // can be number or string
    val coordinates: Coordinates = Coordinates(),
    val timezone: Timezone = Timezone()
)

data class Street(
    val number: Int = 0,
    val name: String = ""
)

data class Coordinates(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
)

data class Timezone(
    val offset: String = "",
    val description: String = ""
)

data class Login(
    val uuid: String = "",
    val username: String = "",
    val password: String = "",
    val salt: String = "",
    val md5: String = "",
    val sha1: String = "",
    val sha256: String = ""
)

data class DateOfBirth(
    val date: String = "",
    val age: Int = 0
)

data class Registered(
    val date: String = "",
    val age: Int = 0
)

data class Id(
    val name: String = "",
    val value: String = ""
)

data class Picture(
    val large: String = "",
    val medium: String = "",
    val thumbnail: String = ""
)