package com.chipcerio.endlessscrolldemo

import io.reactivex.Observable

interface UserSource {
    fun getUsers(page: Int, results: Int, seed: String): Observable<List<User>>
}
